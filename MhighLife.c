#include <stdio.h>
#include <stdlib.h>
#include<mpi.h> 
//
void Mpihighlife(int procs,int size,int hf[][size],int thf[][size])
{
	 
	for(int jj=0;jj<size;jj+=procs){
			thf[0][jj]=thf[0][jj];
			thf[size-1][jj]=thf[size-1][jj];
	}
	for(int ii=1;ii<size-1;ii+=procs)
	{
		for(int jj=1;jj<size-1;jj+=procs)//
		{		
			int count =thf[(ii-1)][jj-1]+thf[(ii-1)][jj]+
			thf[(ii-1)][jj+1]
			+thf[ii][jj-1] +thf[ii][jj+1]+thf[(ii+1)][jj-1]
			+thf[(ii+1)][jj]+thf[(ii+1)][jj+1];
			if(count ==2 || count ==3 )
				thf[ii][jj]=1;
			else if(count ==6 )
				thf[ii][jj]=thf[ii][jj];
			else
				thf[ii][jj]=0;
			
				
		}
		}
	
	
}		

int main(int argc, char** argv)
{
	int size;
	int i,j;
	if(argc>1){
		size=strtol(argv[1],NULL,16);
	}
	int iter=0,myid = 0, procs = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
	MPI_Status status;
	int hf[size+2][size+2];
	int hh[size+2][size+2];
	if(myid==0){
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			hf[i][j]=rand()%2; 
		}
	}
	if(procs>1)
    {
      MPI_Send(hf,size*size,MPI_INT,myid+1,99,MPI_COMM_WORLD);
      printf("%d have send to %d\n",myid,myid+1);   
    }
	}
	else{
	MPI_Recv(hf,size*size,MPI_INT,myid-1,99,MPI_COMM_WORLD,&status);
    printf("%d have recv from %d\n",myid,myid-1);
    if(myid<procs-1)
    {
      MPI_Send(hf,size*size,MPI_INT,myid+1,99,MPI_COMM_WORLD);
      printf("%d have send to %d\n",myid,myid+1);
    }
	}
	MPI_Barrier(MPI_COMM_WORLD);
	//compute block by procs
	if(size%procs!=0){
		printf("size should be div by procs\n");
		MPI_Finalize();
		return 0;
	}
	//copy oper
	for(int ii=0;ii<size;ii+=procs){
		for(int jj=0;jj<size;jj+=procs){
			hh[ii][jj]=hf[ii][jj];
		}
	}
	while(1){	
	Mpihighlife(procs,size,hf,hh);
	if(iter>300)break;
	iter++;
	}
	//copy oper
	for(int ii=0;ii<size;ii+=procs){
		for(int jj=0;jj<size;jj+=procs){
			hf[ii][jj]=hh[ii][jj];
		}
	}
	
	if(myid==0){
	for(i=0;i<size;i++)
	{
		for(j=0;j<size;j++)
		{
			if(hf[i][j]==1){
				printf(" 1 ");
			}
			else{
				printf(" O ");
			}
		}
		printf("\n");
	
	}
	}
	MPI_Finalize();
	//
	return 0;
}
